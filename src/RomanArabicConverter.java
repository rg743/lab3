import javafx.application.Application;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.stage.*;
import javafx.scene.layout.*;

public class RomanArabicConverter extends Application{

    public void start(Stage primaryStage){

	Label romanLbl = new Label("Roman Numerals: ");
	Label arabicLbl = new Label("Arabic Numerals: ");
	TextField romanTF = new TextField("X");
	TextField arabicTF = new TextField("10");
	Button toRomanBtn = new Button("Convert to Roman");
	Button toArabicBtn = new Button("Convert to Arabic");

	toRomanBtn.setOnAction((e) -> {
		String arabic = arabicTF.getText();
		String roman =
			NumberConversion.arabicToRoman(Integer.parseInt(arabic));
		romanTF.setText(roman);
	    });
	
	Pane romanPane = new HBox();
	romanPane.getChildren().add(romanLbl);
	romanPane.getChildren().add(romanTF);

	Pane arabicPane = new HBox();
	arabicPane.getChildren().add(arabicLbl);
	arabicPane.getChildren().add(arabicTF);

	Pane buttonPane = new HBox();
	buttonPane.getChildren().add(toRomanBtn);
	buttonPane.getChildren().add(toArabicBtn);

	Pane pane = new VBox();
	pane.getChildren().add(romanPane);
	pane.getChildren().add(arabicPane);
	pane.getChildren().add(buttonPane);

	Scene scene = new Scene(pane);
	primaryStage.setTitle("Roman-Arabic Converter");
	primaryStage.setScene(scene);
	primaryStage.show();
    }
}
